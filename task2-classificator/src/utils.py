from transformers import AutoTokenizer, Trainer
import torch


def tokenize_function(batch, max_length=512, model_name="xlm-roberta-base"):
    """
    Tokenizes a batch of text.

    Args:
        batch (dict): A dictionary containing the text to be tokenized.
        tokenizer (AutoTokenizer): A tokenizer instance from the Hugging Face library.
        max_length (int, optional): The maximum length of the tokenized sequence. Default: 512.

    Returns:
        dict: A dictionary containing the tokenized input sequences.
    """
    tokenizer = AutoTokenizer.from_pretrained(model_name)
    tokenized_batch = tokenizer(
        batch["text"],
        padding="max_length",
        truncation=True,
        max_length=max_length,
        return_tensors="pt",
    )
    return tokenized_batch


def concat_title_perex(example):
    """
    Concatenates the title and perex of an example.

    Args:
        example (dict): A dictionary containing a title and perex.

    Returns:
        dict: A dictionary containing the concatenated title and perex as text.
    """
    title = example["title"] if example["title"] else ""
    perex = example["perex"] if example["perex"] else ""
    return {"text": title + " " + perex}


class CustomTrainer(Trainer):
    """
    A custom Trainer class with support for class weights in the loss computation.

    Args:
        class_weights (torch.Tensor, optional): A tensor containing the weights for each class.
    """

    def __init__(self, *args, class_weights=None, **kwargs):
        super().__init__(*args, **kwargs)
        self.class_weights = (
            class_weights.to(self.args.device) if class_weights is not None else None
        )

    def set_weights(self, class_weights):
        """
        Sets the class weights.

        Args:
            class_weights (torch.Tensor): A tensor containing the weights for each class.
        """
        self.class_weights = class_weights.to(self.args.device)

    def compute_loss(self, model, inputs, return_outputs=False):
        """
        Computes the custom loss for the model given the inputs.

        Args:
            model: The model to compute the loss for.
            inputs (dict): A dictionary containing the input tensors.
            return_outputs (bool, optional): Whether to return the model outputs. Default: False.

        Returns:
            tuple: A tuple containing the computed loss and model outputs (if return_outputs is True).
        """
        labels = inputs.get("labels")
        # forward pass
        outputs = model(**inputs)
        logits = outputs.get("logits")
        # compute custom loss (suppose one has 3 labels with different weights)

        loss_fct = torch.nn.CrossEntropyLoss(weight=self.class_weights)
        loss = loss_fct(logits.view(-1, self.model.config.num_labels), labels.view(-1))
        return (loss, outputs) if return_outputs else loss
