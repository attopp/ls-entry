import pytest
from transformers import AutoTokenizer
from src import tokenize_function, concat_title_perex, CustomTrainer


def test_tokenize_function():
    tokenizer = AutoTokenizer.from_pretrained("xlm-roberta-base")
    batch = {"text": ["Hello, world!", "This is a test."]}
    tokenized_batch = tokenize_function(batch, tokenizer)

    assert "input_ids" in tokenized_batch
    assert "attention_mask" in tokenized_batch
    assert len(tokenized_batch["input_ids"]) == 2
    assert len(tokenized_batch["attention_mask"]) == 2


def test_concat_title_perex():
    example = {"title": "Title", "perex": "Perex"}
    result = concat_title_perex(example)

    assert "text" in result
    assert result["text"] == "Title Perex"


def test_custom_trainer_init():
    trainer = CustomTrainer(class_weights=None)
    assert trainer.class_weights is None


def test_custom_trainer_set_weights():
    trainer = CustomTrainer(class_weights=None)
    weights = torch.tensor([1.0, 1.0, 1.0])
    trainer.set_weights(weights)

    assert trainer.class_weights is not None
    assert torch.equal(trainer.class_weights, weights)
