from bs4 import BeautifulSoup
import re
import requests
import os
import json
from datetime import datetime

data_file = "../data/data.jsonl"
test_set = "../data/test.jsonl"
prompt_template = "Generate a maximum 300 word long article about the following match. You have to incorporate these details: the score: {score}, date: {date}, place: {place}, the teams: {team1} and {team2}. Here are the match comments: {comments} The article has to be written in Czech language."


def preprocess_ls_match_data(match_id="StyIyb9D", x_fsign="SW9D1eZo"):
    """
    Preprocess live sport match data for a given match ID and X-FSign header value.

    Args:
        match_id (str): The ID of the match to preprocess data for.
        x_fsign (str): The X-FSign header value.

    Returns:
        dict: A dictionary containing the preprocessed match data.
    """
    # Comments
    url_comments = "https://d.livesport.cz/x/feed/df_lc_1_" + match_id

    # Stats
    url_stats = "https://d.livesport.cz/x/feed/dc_1_" + match_id

    # Article
    url_article = "https://d.livesport.cz/x/feed/df_mr_1_" + match_id

    headers = {
        "authority": "d.livesport.cz",
        "method": "GET",
        "scheme": "https",
        "accept": "*/*",
        "accept-encoding": "gzip, deflate, br",
        "accept-language": "cs-CZ,cs;q=0.9,en;q=0.8 ",
        "cache-control": "no-cache",
        "origin": "https://www.livesport.cz",
        "pragma": "no-cache",
        "referer": "https://www.livesport.cz/",
        "sec-ch-ua": '"Chromium";v="112", "Google Chrome";v="112", "Not:A-Brand";v="99"',
        "sec-ch-ua-mobile": "?0",
        "sec-fetch-dest": "empty",
        "sec-fetch-mode": "cors",
        "sec-fetch-site": "same-site",
        "user-agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/112.0.0.0 Safari/537.36",
        "x-fsign": x_fsign,  # Get this sign from the browser
    }

    response_article = requests.get(url_article, headers=headers)
    response_stats = requests.get(url_stats, headers=headers)
    response_comments = requests.get(url_comments, headers=headers)

    s_article = BeautifulSoup(response_article.content, from_encoding="utf-8")
    s_stats = BeautifulSoup(response_stats.content, from_encoding="utf-8")
    s_comments = BeautifulSoup(response_comments.content, from_encoding="utf-8")

    pattern_comments = r"MD÷(.*?)¬"
    pattern_times = r"MB÷(.*?)¬"
    pattern_article_title = r"PV÷(.*?)¬"
    pattern_article_text = r"PT÷CO¬PV÷((?:.|\s)*?)(?=¬TE÷CO¬TS)"

    pattern_goals_home_team = r"DG÷(.*?)¬"
    pattern_goals_away_team = r"DH÷(.*?)¬"

    t_article = s_article.prettify()
    t_article = re.sub(
        r"https?:\/\/(?:www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b(?:[-a-zA-Z0-9()@:%_\+.~#?&\/=]*)",
        "",
        t_article,
    )

    t_article = re.sub(r"\[.*?\]", "", t_article)
    t_stats = s_stats.prettify()
    t_comments = s_comments.prettify()

    # Find all occurrences of the pattern
    article_title = re.search(pattern_article_title, t_article)
    article_text = re.search(pattern_article_text, t_article)
    raw_comments = re.findall(pattern_comments, t_comments)
    raw_times = re.findall(pattern_times, t_comments)
    score_home_team = re.search(pattern_goals_home_team, t_stats)
    score_away_team = re.search(pattern_goals_away_team, t_stats)

    if article_title:
        article_title = article_title.group(1)
    if article_text:
        article_text = article_text.group(0).replace("PT÷CO¬PV÷", "")
    if score_home_team:
        score_home_team = score_home_team.group(1)
    if score_away_team:
        score_away_team = score_away_team.group(1)

    comments = [comment.replace("MD÷", "") for comment in raw_comments]
    times = [time.replace("MB÷", "") for time in raw_times]

    comments_structured = []
    for idx, t in enumerate(times):
        comments_structured.append(t + " - " + comments[idx])

    new_record = {
        "id": match_id,
        "date": "",
        "place": "",
        "team1": "",
        "team2": "",
        "score": f"{score_home_team}:{score_away_team}",
        "comments": "".join(comments_structured),
        "completion": article_title + ":\n" + article_text,
    }
    save_to_jsonl(new_record)
    return new_record


def scrape_match_ids(x_fsign="SW9D1eZo"):
    """
    !!UNFINISHED!!
    Scrape match IDs from the live sport website using the given X-FSign header value.

    Args:
        x_fsign (str): The X-FSign header value.
    """
    url = "https://d.livesport.cz/x/feed/f_1_0_2_cs_1"
    headers = {
        "authority": "d.livesport.cz",
        "method": "GET",
        "scheme": "https",
        "accept": "*/*",
        "accept-encoding": "gzip, deflate, br",
        "accept-language": "cs-CZ,cs;q=0.9,en;q=0.8 ",
        "cache-control": "no-cache",
        "origin": "https://www.livesport.cz",
        "pragma": "no-cache",
        "referer": "https://www.livesport.cz/",
        "sec-ch-ua": '"Chromium";v="112", "Google Chrome";v="112", "Not:A-Brand";v="99"',
        "sec-ch-ua-mobile": "?0",
        "sec-fetch-dest": "empty",
        "sec-fetch-mode": "cors",
        "sec-fetch-site": "same-site",
        "user-agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/112.0.0.0 Safari/537.36",
        "x-fsign": x_fsign,  # Get this sign from the browser
    }
    response = requests.get(url, headers=headers)
    s_response = BeautifulSoup(response.content, from_encoding="utf-8")
    # TODO: Finish this function to scrape the match IDs


def save_to_jsonl(new_record, id_key="id", test_set=test_set, file_name=data_file):
    """
    Save a new record to a JSONL file, and update the test set.

    Args:
        new_record (dict): The new record to save.
        id_key (str): The key of the ID field in the record dictionary.
        test_set (str): The path of the test set JSONL file.
        file_name (str): The path of the data JSONL file.
    """
    data = []
    test = []

    # If the JSONL file exists, read it and load the data into a list
    if os.path.exists(file_name):
        with open(file_name, "r", encoding="utf-8") as file:
            for line in file:
                data.append(json.loads(line))

    # Check if the record with the given ID exists in the list
    record_exists = any(record[id_key] == new_record[id_key] for record in data)

    # If the record doesn't exist, append it to the list
    if not record_exists:
        data.append(
            {
                "id": new_record["id"],
                "prompt": prompt_template.format(**new_record),
                "completion": new_record["completion"],
            }
        )
        test.append(
            {
                "prompt": prompt_template.format(**new_record),
                "completion": new_record["completion"],
            }
        )

        # Write the updated list back to the JSONL file
        with open(file_name, "w", encoding="utf-8") as file:
            for record in data:
                file.write(json.dumps(record, ensure_ascii=False) + "\n")
        # Write the updated list back to the JSONL file
        with open(test_set, "w", encoding="utf-8") as file:
            for record in test:
                file.write(json.dumps(record, ensure_ascii=False) + "\n")
