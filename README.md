# Klasifikace sportovních článků a generování článků

## Požadavky

- Python 3+
- Knihovny: vše v requirements.txt

## Instalace

1. Nainstalujte Python 3
2. Nainstalujte potřebné knihovny pomocí pip a requirements.txt
